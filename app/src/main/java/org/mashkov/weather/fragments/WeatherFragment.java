package org.mashkov.weather.fragments;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.mashkov.weather.R;
import org.mashkov.weather.Utils;
import org.mashkov.weather.models.APIError;
import org.mashkov.weather.models.WeatherResponse;
import org.mashkov.weather.retrofit.ApiClient;
import org.mashkov.weather.retrofit.ErrorParser;
import org.mashkov.weather.retrofit.ServiceGenerator;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherFragment extends Fragment
        implements Callback<WeatherResponse>, OnMapReadyCallback {

    private final String OWM_API_KEY = "4a817815ddffc203e7ef99476becf971";
    private ScrollableMapFragment fragment;
    private LatLng location;
    private EditText locationQuery;
    private ImageView weatherImage;
    private TextView locationName, temperature;
    private TableLayout tablelayout;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_weather, container, false);
        tablelayout = (TableLayout)v.findViewById(R.id.table_layout);
        colorizeTable();
        progressBar = (ProgressBar) v.findViewById(R.id.circular_progress_bar);
        weatherImage = (ImageView)v.findViewById(R.id.weather_image);
        locationQuery = (EditText)v.findViewById(R.id.search_query);
        locationQuery.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    getWeather(locationQuery.getText().toString());
                    return true;
                }
                return false;
            }
        });
        locationName = (TextView)v.findViewById(R.id.location_name);
        temperature = (TextView)v.findViewById(R.id.temperature);
        ImageButton find = (ImageButton) v.findViewById(R.id.find);
        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWeather(locationQuery.getText().toString());
            }
        });

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final ScrollView scrollView = (ScrollView) view.findViewById(R.id.scroll_view);
        fragment = (ScrollableMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        fragment.setListener(new ScrollableMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                scrollView.requestDisallowInterceptTouchEvent(true);
            }
        });
    }

    @Override
    public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
        progressBar.setVisibility(View.GONE);
        showMainInfo();
        locationQuery.setText("");
        if(response.isSuccessful()){
            hideKeyboard();
            location = new LatLng(response.body().getCoord().getLat(),
                    response.body().getCoord().getLon());
            fragment.getMapAsync(this);
            Picasso.with(getActivity())
                    .load("http://api.openweathermap.org/img/w/" + response.body().getWeather()[0].getIcon() + ".png")
                    .into(weatherImage);
            locationName.setText("Weather in " + response.body().getName() + ", " + response.body().getSys().getCountry());
            temperature.setText(getString( R.string.temperature_placeholder,response.body().getMain().getTemp()));
            ((TextView)((TableRow)tablelayout.getChildAt(0)).getChildAt(1)).setText(response.body().getWeather()[0].getMain());
            ((TextView)((TableRow)tablelayout.getChildAt(1)).getChildAt(1)).setText(response.body().getWeather()[0].getDescription());
            ((TextView)((TableRow)tablelayout.getChildAt(2)).getChildAt(1)).setText(getString( R.string.pressure_placeholder, response.body().getMain().getPressure()));
            ((TextView)((TableRow)tablelayout.getChildAt(3)).getChildAt(1)).setText(getString( R.string.humidity_placeholder, response.body().getMain().getHumidity()));
            ((TextView)((TableRow)tablelayout.getChildAt(4)).getChildAt(1)).setText(getString( R.string.temperature_placeholder, response.body().getMain().getTemp_min()));
            ((TextView)((TableRow)tablelayout.getChildAt(5)).getChildAt(1)).setText(getString( R.string.temperature_placeholder, response.body().getMain().getTemp_max()));
            ((TextView)((TableRow)tablelayout.getChildAt(6)).getChildAt(1)).setText(getString( R.string.pressure_placeholder, response.body().getMain().getSea_level()));
            ((TextView)((TableRow)tablelayout.getChildAt(7)).getChildAt(1)).setText(getString( R.string.pressure_placeholder, response.body().getMain().getGrnd_level()));
            ((TextView)((TableRow)tablelayout.getChildAt(8)).getChildAt(1)).setText(getString( R.string.wind_speed_placeholder,response.body().getWind().getSpeed()));
            String wind = Utils.degreesToDirection(response.body().getWind().getDeg());
            ((TextView)((TableRow)tablelayout.getChildAt(9)).getChildAt(1)).setText(wind);
            ((TextView)((TableRow)tablelayout.getChildAt(10)).getChildAt(1)).setText(getString(R.string.clouds_placeholder, response.body().getClouds().getAll()));
            String sunrise = Utils.timestampToTime(response.body().getSys().getSunrise());
            ((TextView)((TableRow)tablelayout.getChildAt(11)).getChildAt(1)).setText(sunrise);
            String sunset = Utils.timestampToTime(response.body().getSys().getSunset());
            ((TextView)((TableRow)tablelayout.getChildAt(12)).getChildAt(1)).setText(sunset);
            ((TextView)((TableRow)tablelayout.getChildAt(13)).getChildAt(1)).setText(response.body().getLatLon());
        } else {
            APIError error = ErrorParser.parseError(response);
            Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(Call<WeatherResponse> call, Throwable t) {
        progressBar.setVisibility(View.GONE);
        showMainInfo();
        locationQuery.setText("");
        Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(location);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        googleMap.addMarker(markerOptions);

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(location)
                .zoom(10.0f)
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public void colorizeTable() {
        int childViewCount = tablelayout.getChildCount();
        for (int i = 0; i < childViewCount; i++) {
            TableRow row = (TableRow) tablelayout.getChildAt(i);
            for (int j = 0; j < row.getChildCount(); j++) {
                TextView tv = (TextView) row.getChildAt(j);
                if (i % 2 == 0) {
                    tv.setBackground(new ColorDrawable(0xFFECEFF1));
                }
            }
        }
    }

    private void hideKeyboard(){
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void getWeather(final String location){
        hideMainInfo();
        progressBar.setVisibility(View.VISIBLE);
        Map<String,String> params = new HashMap<String, String>(){{
            put("q", location);
            put("appid", OWM_API_KEY);
            put("units","metric");
        }};
        ApiClient client = ServiceGenerator.createService(ApiClient.class);
        Call<WeatherResponse> call = client.getWeatherInfo(params);
        call.enqueue(WeatherFragment.this);
    }

    private void hideMainInfo(){
        weatherImage.setVisibility(View.INVISIBLE);
        temperature.setVisibility(View.INVISIBLE);
        locationName.setVisibility(View.INVISIBLE);
    }

    private void showMainInfo(){
        weatherImage.setVisibility(View.VISIBLE);
        temperature.setVisibility(View.VISIBLE);
        locationName.setVisibility(View.VISIBLE);
    }
}