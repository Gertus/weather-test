package org.mashkov.weather.retrofit;

import org.mashkov.weather.models.WeatherResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ApiClient {

    @GET("/data/2.5/weather")
    Call<WeatherResponse> getWeatherInfo(@QueryMap Map<String, String> queries);

}
